---
title: Sharing CI/CD pipelines between microservices. Part 2
identifier: Part 2
---
This post describes a solution to pipeline sharing problem based on [git subtree](https://github.com/git/git/blob/master/contrib/subtree/git-subtree.txt). I will copy conclusions from [part 1](../part1) here as a reminder:

- it's necessary for repositories to be self-contained: every project repository has its own copy of pipeline files, they don't reference some global pipeline
- pipeline files should be introduced to a repository with some kind of a tool. No manual copy-paste
- there needs to be a way to update pipelines in every project preserving custom changes without resorting to editing files manually

#### Git subtree
Git subtree is a tool that comes with git, so you already have it installed. It consists of 5 commands that start with "git subtree" although for the sake of this article we only need two of them.

The command 
```bash
git subtree add -P ci --squash "https://github.com/myorg/standard-pipeline.git" master
``` 
downloads all files from _master_ branch of repository "standard-pipeline" and copies them to a local folder "ci":

![git subtree add](../git_subtree_add_filesystem.svg)

So it sounds like copy-paste but not exactly - git makes a squashed commit that represents standard-pipeline repository and then merges this commit with the HEAD of your repository. The result git history would look like this:

![git history](../git_subtree_add.svg)

Now it's obvious from the git history where those pipeline files came from - from another repository! No manual copy-pasting!

The end result is no different from manually creating a branch with all pipeline files and merging the branch to the master. The pipeline files now are part of your repository, you can modify them however you want. The repository **owns** the files.
But this is only the beginning, _git subtree_ starts to shine when you need to update your pipeline from standard-pipeline.

The command
```bash
git subtree pull -P ci --squash "https://github.com/myorg/standard-pipeline.git" master
``` 
takes all changes from standard-pipeline since your last _git subtree add_ or _pull_, squashes them and merges them again to your repository.

![git subtree add](../git_subtree_pull.svg)

It's not just a copy-paste of the files, it's git - so it does git merge. 

Imagine you changed a pipeline file "build.sh" in the commit "do something useful" between _git subtree add_ and _pull_. Git will not lose your change because it's the same as merging any other branch with git. 

What if you have changed the same line of build.sh as standard-pipeline did? As always with git - you will get a merge conflict, you have to resolve the conflict manually and that's a good thing because you can't leave such an important decision to a script. The beauty of this is that once you resolved the conflict you can run _git subtree pull_ to add more changes that were added since the last time and git won't ask you to resolve the same conflict again - just like when you work with regular branches.

Git subtree looks like copy-paste on steroids: you get a copy of the files from the other repository and an additional link between these files and that other repository. This link allows you to update the copied files intelligently preserving you local changes. In the end the repository owns the files, controls everything, it's self-contained and it can easily get the latest updates from a single place.

Git subtree might be the best way to distribute CI/CD pipelines. .Net ecosystem has nuget packages, nodejs has npm packages, Rust has crates, etc. Pipelines have _git subtree_

#### One last practical issue
In the [part 1](../part1) we discussed using a common parameterized pipeline. It has a powerful advantage - the changes in the pipeline affect all projects immediately. It's a twofold thing though: the change can break all pipelines or it can add value.

If you have hundreds or thousands of projects and you make a change to the standard-pipeline that every project should use asap you'd have to run _git subtree pull_ for every project - that will take a few days probably. What if there was a tool that can do it for you. 

There actually is - _subtree-updater_, let's take a look at it in [part 3](../part3)