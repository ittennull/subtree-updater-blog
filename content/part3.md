---
title: Sharing CI/CD pipelines between microservices. Part 3
identifier: Part 3
---
In [part 2](../part2) we learned that _git subtree_ is a great way to share a pipeline but it doesn't scale well. If there are too many microservices and you want to update their pipelines you'd have to run _git subtree pull_ manually for all of them.

#### subtree-updater
[subtree-updater](https://gitlab.com/ittennull/subtree-updater/wikis/Home) is a console tool that takes url of the standard-pipeline, checks out repositories one by one and runs _git subtree pull_ for them, then it creates pull requests and outputs links to them. The only thing left is to open the links and approve/complete the pull requests. It's certainly easier than doing everything manually. 

_subtree-updater_ can optionally complete pull requests too but this decision is up to a particular team: if you want full control you don't want somebody else (a tool or a human) to change something in your repository behind your back, you want to say the last word yourself.

_subtree-updater_ works with GitLab, GitHub and Azure DevOps. 

##### How does it work
Suppose you updated "standard-pipeline" repository and changed container registry URL because the old registry is not available anymore. Surely you want all projects switch to using this new registry right away. You run _subtree-updater_:
```bash
docker run registry.gitlab.com/ittennull/subtree-updater \
    --pat "<personal_access_token>" \
    --subtree-url "https://github.com/myorg/standard-pipeline.git" \
    --exact-location "/ci"
```
It will run `git subtree pull -P ci --squash "https://github.com/myorg/standard-pipeline.git" master` for every repository and print out results like this:
```text
Created or updated PRs:
✓	MyRepository1 - https://github.com/myorg/my-repository1/pull/23
✓	MyRepository2 - https://github.com/myorg/my-repository2/pull/6
✓	MyRepository3 - https://github.com/myorg/my-repository3/pull/250

Merge conflicts or other errors:
✕	MyRepository4 - subtree path: 'ci'
✕	MyRepository5 - subtree path: 'ci'
```

Now every team in your company receives a notification that a pull request for their repository has been created, they can go and approve it.

If you add flag `--complete-pr true` pull request will be completed automatically if possible. The completion can fail if a repository is set up to have minimum number of approvals or you need to wait until some pre-merge checks are done.

Note that you need to provide a Private Access Token (PAT) to _subtree-updater_ - it will use it to work with repositories. The pull requests will be created on behalf of the person to whom the PAT belongs to.

An organization might use a few different languages like java, c#, go, etc. Every language needs their own standard-pipeline and when you run _subtree-updater_ you want to limit the repositories that are being updated to those that actually have the pipeline that you propagate changes from. It's possible to do with filter-parameters of _subtree-updater_. For example to take into account only repositories whose name contains the word "java" add this parameter: `--repository-filters "*java*"`. To filter repositories with name starting with "proxy" or ending with "service": `--repository-filters "proxy*,*service"`. Alternatively you can use filters that work with groups, for example take all repositories in a GitLab group "account": `--gitlab-group-filters "account"`

If a repository doesn't have a pipeline added with a _git subtree add_ or it was a different pipeline from another repository - _subtree-updater_ will not add pipeline because it only updates existing pipelines and never adds them. Of course in this case no pull request will be created and no changes to the repository. That means that you don't have to use the above-mentioned filters - if something doesn't match _subtree-updater_ simply skips that repository but filters are still useful because they save time - no need to check out all 200 repositories if you know that only 15 of them use the pipeline you have just changed.

As you can see from the output some repositories didn't get pull requests because there were merge conflicts, you actually need to run _git subtree pull_ manually for these repositories and resolve the conflicts.

#### Conclusion
We looked into different ways of sharing CI/CD pipelines and discovered that _git subtree_ offers a very good alternative to common approaches. Finally _subtree-updater_ was introduced to allow you to run _git subtree pull_ for many repositories and save lots of time. Please read the [documentation](https://gitlab.com/ittennull/subtree-updater/wikis/Home) of _subtree-updater_ to learn more about it