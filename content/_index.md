### Sharing CI/CD pipelines between microservices with _git-subtree_ and _subtree-updater_

[Part 1](part1) describes common approaches to sharing pipelines with pros and cons

[Part 2](part2) presents the perfect approach based on _git subtree_ that solves all the problem of the others except for one

[Part 3](part3) introduces _subtree-updater_ that fixes that one last problem