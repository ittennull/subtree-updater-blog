---
title: About
---
_subtree-updater_ source code - https://gitlab.com/ittennull/subtree-updater

If you have any questions or suggestions - feel free to create an issue, I will be happy to help you.

---
<i data-feather="gitlab"></i>
My GitLab account - https://gitlab.com/ittennull

<i data-feather="linkedin"></i>
My LinkedIn - https://www.linkedin.com/in/dmitrysemenov0