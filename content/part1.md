---
title: Sharing CI/CD pipelines between microservices. Part 1
identifier: Part 1
---
Many systems nowadays are built using microservice approach which means there are many separately deployable services. Each one of them needs to have a CI/CD pipeline: a set of scripts, yaml files and so on - everything that builds the code, tests and deploys it. It's not a problem when there are just a few services - any approach works. 

However, what if your company is Netflix-size one with hundreds or thousands of microservices. Now there is a problem - you need to have pipelines for all of them. Many microservices are very much alike: they are small, written in the same language, have similar build/deploy requirements - all that means that their pipelines are going to be almost the same. Probably not 100% same because each service has at least a unique name, different scale out parameters, some unique environment variables, etc. Moreover, some services are different and will need to have something special in their pipelines but usually the core of the pipeline remains.

#### Standard pipeline
Having a repository with a standard pipeline is very helpful because you can define the main build steps there, add standard policies (f.ex. don't publish if tests fail) and provide an excellent starting point for new services. It can be a single file "build.sh" or 100 files depending on your needs. Standard pipeline can be compared to a framework - it defines a way but allows you to customize certain aspects. 

#### Different ways of sharing pipelines
There are a few ways with different pros and cons. Let's discuss them to understand better what characteristics an ideal pipeline sharing approach should have.

##### One pipeline to rule them all
Here we have one pipeline defined somewhere else be it in jenkins, gitlab-ci, it doesn't matter. This pipeline is used by every project and thus it's parameterized to support all possible requirements. Sounds good but there are downsides. For one - there might be too many different parameters to accommodate to all projects which makes the pipeline difficult to extend and support: you never know if a parameter still needed by some project; imagine you need to change something - how would you know it won't break some of the projects. Also many aspects require configuration now and using the pipeline is difficult because this configuration must be provided somewhere. Moreover it's a single point of failure - a small bug can break deployment of everything across a company and seriously hinder productivity. The last one is critical - it's unacceptable that your pipeline breaks because somebody else changed something in a separate repository. It's always the case when a project is not self-contained. A pipeline must be a part of repository that uses it.

##### Copy-paste
Take files from the standard pipeline repository, copy them to every other repository and make those small adjustments like setting the correct application name, etc. Simple enough but what to do if standard pipeline changes. For example a new security check was added that must pass before a service allowed to be deployed. You can copy-paste these changes to every repository exactly to the right place (hopefully it's only one file). What if there are several changes in one file - in this case it would be faster to just copy the new file over the old one. Could work unless you adjusted this file in a specific repository and the new file will overwrite everything. You can check the history of this particular file before replacing it but it'll take a lot of time to do this for every repository.

On the other hand copy-paste is good because the files from standard pipeline are now part of a repository - this repository **owns** the files and nobody can interfere or change anything without modifying the repository. The pipeline won't break simply because somebody made a mistake somewhere else. The repository is self-contained.

##### Pipeline generator
Another variation is to use a generator tool, that takes a pipeline template, a bunch of parameters specific for a concrete project and outputs a pipeline. Unfortunately this tool must be complex and have too many parameters, same as in "one pipeline for everything" approach. Also if you change some pipeline file in your repository the generator tool wouldn't be able to update the file without losing your change.

##### Git submodule
Standard pipeline can be included to every project as [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules). The submodule version is fixed, nobody can change it without modifying the repository code, which is good but there is no room to add customization: if you change code in submodule you need to commit it back to the main repository because it doesn't belong to your repository, it's just a submodule. You can commit to a branch of a repository with standard pipeline but then there would be hundreds of branches per project and total mess. Not to mention that git submodules are notoriously difficult to work with because it's easy to make a mistake.

#### Ideal approach?
Let's summarize what an ideal pipeline sharing approach must have:

- it's necessary for repositories to be self-contained: every project repository has its own copy of pipeline files, they don't reference some global pipeline
- pipeline files should be introduced to a repository with some kind of a tool. No manual copy-paste
- there needs to be a way to update pipelines in every project preserving custom changes without resorting to editing files manually

Lucky us, _git subtree_ is a perfect fit. Read [part 2](../part2) to see how it works